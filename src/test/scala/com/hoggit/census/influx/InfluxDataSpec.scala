package com.hoggit.census.influx

import com.hoggit.census.geoip._
import java.time.Instant
import org.scalatest.{ FlatSpec, Matchers }

class InfluxDataSpec extends FlatSpec with Matchers {

  def geoIpInfo(
    ip: String = "127.0.0.1",
    country: String = "Canada",
    region: String = "Ontario"
  ) = GeoIPInfo(ip, country, region)

  "fromGeoIpInfos" should "convert a list of GeoIPInfo to a list of InfluxData" in {
    //3 Canada + 3 United states
    val geoIps = List(
      geoIpInfo(),
      geoIpInfo(region = "Quebec"),
      geoIpInfo(),
      geoIpInfo(country = "United States"),
      geoIpInfo(country = "United States"),
      geoIpInfo(country = "United States")
    )
    val i = Instant.now()
    val influxDatas = InfluxData.fromGeoIpInfos(geoIps, i)
    influxDatas.size shouldBe 2
    influxDatas should contain theSameElementsAs List(
      InfluxData("United States", 3, i),
      InfluxData("Canada", 3, i)
    )

  }
}

