package com.hoggit.census.request

import akka.http.scaladsl.model.MediaTypes._
import akka.http.scaladsl.model.{ HttpEntity, StatusCodes }
import akka.http.scaladsl.testkit.ScalatestRouteTest
import java.time.Instant
import org.scalatest.{ FlatSpec, Matchers }
import scala.concurrent.Future
import spray.json._

import com.hoggit.census.geoip._
import com.hoggit.census.{ ConnectedPlayer, ServerPlayerReport }
class ServerReportHandlerSpec extends FlatSpec with Matchers with ScalatestRouteTest {

  case class TestGeoIPLookup(info: GeoIPInfo) extends GeoIPLookup {
    var ips: List[IPAddress] = List()
    def lookup(ip: IPAddress) = {
      ips = ip :: ips
      Future.successful(info.copy(ip = ip))
    }
  }

  "The ReportHandler" should "accept a json body to /players and respond with 200" in {
    val spr = ServerPlayerReport.empty
    val sut = new ServerReportHandler(TestGeoIPLookup(GeoIPInfo.empty))
    Post("/players").withEntity(HttpEntity(`application/json`, spr.toJson.compactPrint)) ~> sut.routes ~> check {
      status shouldBe StatusCodes.OK
    }
  }

  it should "lookup the ips of all players in the ServerReport" in {
    val players = List(
      ConnectedPlayer("Acidic", "ucid", "127.0.0.1"),
      ConnectedPlayer("Zelly", "ucid", "127.0.0.2")
    )
    val report = ServerPlayerReport(Instant.now(), players)
    val geoIP = TestGeoIPLookup(GeoIPInfo.empty)
    val sut = new ServerReportHandler(geoIP)
    Post("/players").withEntity(HttpEntity(`application/json`, report.toJson.compactPrint)) ~> sut.routes ~> check {
      status shouldBe StatusCodes.OK
      geoIP.ips should contain theSameElementsAs List("127.0.0.1", "127.0.0.2")
    }
  }

}
