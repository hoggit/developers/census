package com.hoggit.census

import java.time.Instant
import org.scalatest.{ FlatSpec, Matchers }
import spray.json._

class ConnectedPlayerSpec extends FlatSpec with Matchers {

  "ConnectedPlayer" should "convert to and from json" in {
    val p = ConnectedPlayer("Acidic", "1234", "127.0.0.1")
    p.toJson shouldBe """{
                      |"name": "Acidic",
                      |"ucid": "1234",
                      |"ip": "127.0.0.1"
                      |}""".stripMargin.parseJson
  }

  "ServerPlayerReport" should "convert to and from json" in {
    val ps = List(
      ConnectedPlayer("Acidic", "1234", "127.0.0.1"),
      ConnectedPlayer("Zelly", "4321", "127.0.0.2")
    )
    val rep = ServerPlayerReport(Instant.ofEpochMilli(1234567), ps)
    rep.toJson shouldBe """{
                        |"timestamp": 1234567,
                        |"playerList": [
                        |{
                        |"name": "Acidic",
                        |"ucid": "1234",
                        |"ip": "127.0.0.1"
                        |},
                        |{
                        |"name": "Zelly",
                        |"ucid": "4321",
                        |"ip": "127.0.0.2"
                        |}]}""".stripMargin.parseJson
  }
}
