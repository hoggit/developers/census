package com.hoggit.census.geoip

import org.scalatest.{ FlatSpec, Matchers }

import spray.json._
class GeoIPSpec extends FlatSpec with Matchers {
  "GeoIPInfo" should "serialize to and from json" in {
    val gInfo = GeoIPInfo("127.0.0.1", "Canada", "Ontario")

    gInfo.toJson.compactPrint.parseJson.convertTo[GeoIPInfo] shouldBe gInfo
  }

  it should "follow a useable format" in {
    val gInfoString = """{
                      |"ip": "127.0.0.1",
                      |"country_name": "Canada",
                      |"region_name": "Ontario"
                      |}""".stripMargin
    gInfoString.parseJson shouldBe GeoIPInfo("127.0.0.1", "Canada", "Ontario").toJson
  }
}
