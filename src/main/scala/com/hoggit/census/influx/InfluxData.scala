package com.hoggit.census.influx

import com.hoggit.census.geoip._
import java.time.Instant

object InfluxData {
  /**
   * Turns a list of [[GeoIPInfo]] into a list of [[InfluxData]]. The process involves
   * grouping all the countries from the GeoIPInfos, and then creating an InfluxData for each
   * country that appears.
   * @param infos The list of GeoIPInfo to translate.
   * @param ts The timestamp that all the influxdata's will be marked with.
   */
  def fromGeoIpInfos(infos: List[GeoIPInfo], ts: Instant): List[InfluxData] = {
    infos.groupBy(_.countryName).map {
      case (country, infos) => {
        InfluxData(country, infos.size, ts)
      }
    }.toList
  }
}

/**
 * Handles the formatting for sending data to the influxDB. We will be sending it as
 * aggregations by country. So one influx data will represenent a bunch of players
 * from the same country.
 * @see InfluxData.fromGeoIPInfos
 * @param country Which country is being aggregated with this InfluxData
 * @param playerCount The count of the player from the same country
 * @param timestamp An instant to help influxDB account for replays and time-based data.
 */
case class InfluxData(country: String, playerCount: Integer, timestamp: Instant) {
}
