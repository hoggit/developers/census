package com.hoggit.census.influx

import java.time.Instant
import scala.concurrent.Future

import com.hoggit.census.http.HttpClient

trait InfluxDB {
  def influxDataToString(data: InfluxData, ts: Instant): String = {
    /*TODO: The "gaw_dedi" part should be configurable.
    *We'll want to send this data up from all our servers, so we should allow the servers to send an
    *identifier in the [[ServerPlayerReport]]
    */
    s"players_by_country,country=${data.country},host=gaw_dedi,precision=s players=${data.playerCount} ${ts.getEpochSecond()}"
  }
  /**
   * Sends an InfluxData to a configured InfluxDB somewhere.
   * @param data The instance of InfluxData to send to the InfluxDB
   * @return A Future that will be completed when the data has been sent.
   */
  def send(data: InfluxData): Future[Unit]
}

/**
 * An instance of an InfluxDB which will talk to an HTTP Influx server.
 * @param client An HttpClient that this InfluxDB can use to communicate.
 * @param uri The influxDB endpoint to send data to
 */
case class ProductionInfluxDB(client: HttpClient, uri: String) extends InfluxDB {
  def send(data: InfluxData): Future[Unit] = ???
}
