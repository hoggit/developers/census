package com.hoggit.census

import java.time.Instant
import spray.json._

object ConnectedPlayer {
  import DefaultJsonProtocol._
  implicit val jsonFormat: RootJsonFormat[ConnectedPlayer] = jsonFormat3(ConnectedPlayer.apply)
  def empty = ConnectedPlayer("", "", "")
}

case class ConnectedPlayer(name: String, ucid: String, ip: String)

object ServerPlayerReport {
  import DefaultJsonProtocol._
  def empty = ServerPlayerReport(Instant.now(), List())

  implicit object instantJsonFormat extends RootJsonFormat[Instant] {
    def write(i: Instant): JsValue = JsNumber(i.toEpochMilli())
    def read(j: JsValue): Instant = j match {
      case JsNumber(millis) => Instant.ofEpochMilli(millis.toLong)
      case _ => deserializationError("Number expected trying to parse java.time.Instant")
    }
  }
  implicit val jsonFormat = jsonFormat2(ServerPlayerReport.apply)
}
case class ServerPlayerReport(timestamp: Instant, playerList: List[ConnectedPlayer])
