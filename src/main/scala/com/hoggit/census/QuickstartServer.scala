package com.hoggit.census

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import com.hoggit.census.geoip._
import com.hoggit.census.http._
import com.hoggit.census.request.ServerReportHandler
import com.typesafe.config.ConfigFactory
import com.typesafe.scalalogging.StrictLogging

object QuickstartServer extends App with StrictLogging {
  implicit val config = ConfigFactory.load()
  implicit val system = ActorSystem("census")
  implicit val ec = system.dispatcher
  implicit val am = ActorMaterializer()
  val geoIp = IPStackGeoIPLookup(new AkkaHttpClient(), config.getString("ipstack.api_key"))
  val handler = new ServerReportHandler(geoIp)
  handler.bindingFuture
  logger.info("Server Started")
}
