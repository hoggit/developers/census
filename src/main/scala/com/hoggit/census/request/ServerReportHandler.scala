package com.hoggit.census.request

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server._
import akka.http.scaladsl.server.Directives._
import akka.stream.ActorMaterializer
import ch.megard.akka.http.cors.scaladsl.CorsDirectives._
import com.typesafe.scalalogging.StrictLogging

import com.hoggit.census.geoip._
import com.hoggit.census.ServerPlayerReport

class ServerReportHandler(geoIP: GeoIPLookup)(implicit system: ActorSystem) extends StrictLogging with SprayJsonSupport {
  implicit val materializer = ActorMaterializer()
  implicit val ec = system.dispatcher
  lazy val routes: Route = cors() {
    path("players") {
      post {
        entity(as[ServerPlayerReport]) { report =>
          logger.debug(s"Received a player report with ${report.playerList.size} players.")
          val geoIpInfos = report.playerList.map { player =>
            logger.debug(s"Checking player ${player.name} with ip ${player.ip}")
            geoIP.lookup(player.ip) map { geoIpInfo =>
              logger.debug(s"Got info for ${player.name}: [$geoIpInfo]")
            }
          }
          complete(StatusCodes.OK)
        }
      }
    }
  }
  lazy val bindingFuture = Http().bindAndHandle(routes, "0.0.0.0", 8080)
}
