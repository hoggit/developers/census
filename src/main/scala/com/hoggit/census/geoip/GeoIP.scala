package com.hoggit.census.geoip

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.model._
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.ActorMaterializer
import com.hoggit.census.http.HttpClient
import scala.concurrent.{ ExecutionContext, Future }
import spray.json._
object GeoIPInfo {
  def empty = GeoIPInfo("", "", "")

  implicit object geoIPInfoJsonFormat extends RootJsonFormat[GeoIPInfo] {
    def write(geoip: GeoIPInfo): JsValue = JsObject(
      "ip" -> JsString(geoip.ip),
      "country_name" -> JsString(geoip.countryName),
      "region_name" -> JsString(geoip.regionName)
    )

    def read(js: JsValue): GeoIPInfo = {
      js.asJsObject.getFields("ip", "country_name", "region_name") match {
        case Seq(JsString(ip), JsString(country), JsString(region)) =>
          GeoIPInfo(ip, country, region)
        case _ => deserializationError("Expected a GeoIPInfo")
      }
    }
  }
}

case class GeoIPInfo(ip: String, countryName: String, regionName: String)

trait GeoIPLookup {

  type IPAddress = String
  /**
   * Looks up a given IP address for its location.
   * @param ip The ip address to look up
   * @return The country details from the database.
   */
  def lookup(ip: IPAddress): Future[GeoIPInfo]
}

/**
 * A method of using IPStack.com to look up geoip data.
 * @param HttpClent an instance of an [[com.hoggit.census.http.HttpClient]] to handle the http
 * requests
 * @param apiKey IPStack requires an API key.
 */
case class IPStackGeoIPLookup(client: HttpClient, apiKey: String)(implicit ec: ExecutionContext, mat: ActorMaterializer) extends GeoIPLookup with SprayJsonSupport {
  import scala.concurrent.duration._
  val timeout = 1.second
  private def url(ip: String) = s"http://api.ipstack.com/$ip?access_key=$apiKey"

  private def req(ip: IPAddress): HttpRequest = {
    HttpRequest(uri = url(ip))
  }

  def lookup(ip: IPAddress): Future[GeoIPInfo] = {
    val response = client.mkRequest(req(ip))
    response.flatMap { resp =>
      Unmarshal(resp.entity).to[GeoIPInfo]
    }
  }
}
