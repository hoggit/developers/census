package com.hoggit.census.http

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import scala.concurrent.Future

trait HttpClient {
  def mkRequest(req: HttpRequest): Future[HttpResponse]
}

class AkkaHttpClient()(implicit as: ActorSystem) extends HttpClient {
  def mkRequest(req: HttpRequest) = Http().singleRequest(req)
}
